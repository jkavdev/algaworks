import { Component } from "@angular/core";

//temos que definir um template para um componente
//Fazemos isso atravaes do template ou templateUrl
//Template - sera na mesma pagina, entre '``'
//templateUrl - indica um template
@Component({
    selector: 'hello',
    template: `
        <h2>
            Bora que tá funcionando {{caramba}}
        </h2>
    `
})

export class HelloComponent {
    teste = 'caramba.............'
}