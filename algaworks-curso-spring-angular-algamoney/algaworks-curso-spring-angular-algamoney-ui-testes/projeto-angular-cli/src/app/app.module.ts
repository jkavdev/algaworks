import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
//importando os componentes da aplicacao a serem usados
import { HelloComponent } from './hello/hello.component';
import { BemVindoCliComponent } from './bem-vindo-cli/bem-vindo-cli.component';

//Angular utiliza do padrao decorator para realizar a sua configuracao
//declarations - indicamos quais componentes a aplicacao tem
//bootstrap - indica o componente de configuracao, inicial da aplicacao

//temos que realizar o registramento dos componentes da aplicacao a serem usados
@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    BemVindoCliComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
