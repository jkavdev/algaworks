import { Component } from '@angular/core';

// Componente que fara a configuracao do projeto angular
// selector - indica qual o nome que este componente tera para os htmls
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nome = 'Jhonatan'
  idade = 54

  getIdade() {
    return this.idade
  }

  adicionar() {
    console.log(`Adicionando ${this.nome}`)
    console.log('Adicionando ' + this.nome)

    //numero aleatorio
    const numero = Math.round(Math.random() * 100)
    //concatenacao do nome com o numero
    this.nome = this.nome + ' ' + numero
  }

  alterarNome(event: any) {
    console.log(event)
    //valor do input
    console.log(event.target.value)
    this.nome = event.target.value
  }
}
