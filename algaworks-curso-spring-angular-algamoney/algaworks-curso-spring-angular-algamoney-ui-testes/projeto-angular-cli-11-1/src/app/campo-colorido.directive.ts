import { Directive, ElementRef, Renderer2, HostListener, HostBinding, Input } from '@angular/core';

// exportAs: 'campoColorido' - podemos disponibilizar a api da diretiva
// Com isso todo o codigo fica acessivel a quem usar
@Directive({
  selector: '[appCampoColorido]',
  exportAs: 'campoColorido'
})
export class CampoColoridoDirective {

  //indicando que esta propriedade podera ser recebida
  @Input() cor = 'gray'

  // Podemos atribuir uma propriedade a uma propriedade do elemento que estiver usando esta diretiva
  // @HostBinding('style.backgroundColor') - liga esta propriedade com a propriedade do elemento
  @HostBinding('style.backgroundColor') corDeFundo: string

  // elementRef - contem a referencia do elemento html
  // renderer - possui atributos que podera alterar a estrutura de um elemento html
  constructor(
    //Como ja temos a ligacao de alteracao do campo na propriedade 
    //Nao precisamos dos objetos de alteracao de elemento

    // private elementRef: ElementRef,
    // private renderer: Renderer2,
  ) { }

  //Podemos respoder a eventos do elemento que utilizar esta diretiva
  //@HostListener('focus') - estamos de olho no evento focus
  @HostListener('focus') colorir() {
    //o proprio elemento html, input
    // console.log(this.elementRef.nativeElement)
    // this.renderer.setStyle(this.elementRef.nativeElement,
    // 'background-color', 'yellow')

    //como a propriedade esta ligada ao elemento, apenas alteramos
    this.corDeFundo = this.cor
  }

  //@HostListener('focus') - estamos de olho no evento blur
  //quando perder o foco, muda para trasparent
  @HostListener('blur') descolorir() {
    // this.renderer.setStyle(this.elementRef.nativeElement,
    //   'background-color', 'transparent')
    this.corDeFundo = 'transparent'
  }
}
