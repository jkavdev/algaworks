import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-funcionario-form',
  templateUrl: './funcionario-form.component.html',
  styleUrls: ['./funcionario-form.component.css']
})
export class FuncionarioFormComponent {

  nome = 'Jhonatan'
  adicionado = false
  ultimoId = 0

  @Output() funcionarioAdicionado = new EventEmitter()

  alterarNome(nome: string) {
    this.nome = nome
  }

  adicionar(nome: string) {
    this.nome = nome
    this.adicionado = true
    console.log(this.adicionado)

    const funcionario = {
      id: ++this.ultimoId,
      nome: this.nome
    }

    //ao criar o usuario realiza o emissao do evento passando o funcionario
    this.funcionarioAdicionado.emit(funcionario)
  }

}
