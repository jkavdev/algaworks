import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

// Representacao do cliente
class Cliente {
  nome: string
  email: string
  profissao: string
}

@Component({
  selector: 'app-angular-form',
  templateUrl: './angular-form.component.html',
  styleUrls: ['./angular-form.component.css']
})
export class AngularFormComponent {

  constructor() { }

  cliente = new Cliente()

  profissoes = ['Programador', 'Tester', 'Requisitos', 'Outra']
  profissao = 'Outra'

  // form: NgForm - sabemos que vira uma diretiva
  salvar(form: NgForm) {
    //atribuindo valores do form ao cliente
    this.cliente.nome = form.value.nome
    this.cliente.email = form.value.email
    this.cliente.profissao = form.value.profissao

    console.log(this.cliente)
    console.log(form.valid)
  }

}
