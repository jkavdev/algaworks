import { Component } from '@angular/core';

// Componente que fara a configuracao do projeto angular
// selector - indica qual o nome que este componente tera para os htmls
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  funcionarios =[]

  //recebe o funcionario e adiciona no array
  aoAdicionar(funcionario) {
    this.funcionarios.push(funcionario)
  }

  nome ='JKAVDEV'
  dataAniversario = new Date()
  preco = 11223.15
  troco = 1213.15
  
}
