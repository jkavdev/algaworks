import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// ngModel - para utilizalo precisamos importalo do angular/forms
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
//importando os componentes da aplicacao a serem usados
import { BemVindoCliComponent } from './bem-vindo-cli/bem-vindo-cli.component';
import { FuncionarioCardComponent } from './funcionario-card/funcionario-card.component';
import { FuncionarioFormComponent } from './funcionario-form/funcionario-form.component';
//importando diretiva de campo colorido
import { CampoColoridoDirective } from './campo-colorido.directive';
import { AngularFormComponent } from './angular-form/angular-form.component';

//Angular utiliza do padrao decorator para realizar a sua configuracao
//declarations - indicamos quais componentes a aplicacao tem
//bootstrap - indica o componente de configuracao, inicial da aplicacao

//temos que realizar o registramento dos componentes da aplicacao a serem usados
@NgModule({
  declarations: [
    AppComponent,
    BemVindoCliComponent,
    FuncionarioCardComponent,
    FuncionarioFormComponent,
    CampoColoridoDirective,
    AngularFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
