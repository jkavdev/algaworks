import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// arquivo que o angular procurar para carregar os modulos da aplicacao
// AppModule - modulo da aplicacao

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
