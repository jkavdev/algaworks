# ProjetoAngularCli

* Criando projeto com o angular cli

    ng new projeto-angular-cli

* Iniciando o servidor da aplicação

    ng serve   

* Podemos realizar à adição de componentes atráves do Angular CLI

    ng generate component bem-vindo

* Não iremos gerar o arquivo spec, que é um arquivo para testes

    ng generate component bem-vindo --spec=false

* Podemos ainda diminuir o comando    

    ng g c bem-vindo --spec=false

* Adicionando o boostrap

    npm install boostrap@next --save

* adicionando o css do bootstrap ao angular

    "../node_modules/bootstrap/dist/css/bootstrap.css"

* Existem 3 tipos de diretivas

* diretivas `componentes`

    <app-bem-vindo-cli></app-bem-vindo-cli> onde temos todas as funcionalidades do componente bem-vindo

* diretivas `estruturais`

    <h2 *ngIf='logado'></h2>, onde altera a estrutura do DOM, pode haver ou não o componente h2

* diretivas `atributo`

    <h2 [style.color]='red'></h2>, onde alteramos apenas o atribuito, o componente ja existe no DOM, apenas seu valor

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
