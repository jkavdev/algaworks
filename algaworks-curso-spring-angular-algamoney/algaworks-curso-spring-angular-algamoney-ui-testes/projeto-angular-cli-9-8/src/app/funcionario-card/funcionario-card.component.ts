import { Component, OnInit, Input } from '@angular/core';

//styles - podemos adicionar um array de estilos no proprio componente
//  nao indicado quando o estilo ficar muito grande, se sim melhor separar
@Component({
  selector: 'app-funcionario-card',
  templateUrl: './funcionario-card.component.html',
  styleUrls: ['./funcionario-card.component.css'],
  styles: [`
    .card-block {
      text-transform: uppercase;
      color: blue
    }
  `]
})
export class FuncionarioCardComponent {

  // @Input('func') - indica que a propriedade pode ser atribuida, seu um input
  @Input('func') funcionario = { id: 1, nome: 'Jhonatan' }

  //podemos retornar um objeto com todas as classes do componente
  getClassesCss() {
    return ['badge', 'badge-primary']
  }

  //retornando um valor v/f para adicionar a classe de acordo com o nome
  isAdmin() {
    return this.funcionario.nome.startsWith('Jhon')
  }

  //return um objeto chave valor com os dados do estilo
  // 'border-width': this.funcionario.id + 'px', - podemos ter a definicao por ''
  // backgroundColor: this.funcionario.id % 2 === 0 - ou camelCase
  getEstilosCartao() {
    return {
      'border-width': this.funcionario.id + 'px',
      backgroundColor: this.funcionario.id % 2 === 0
        ? 'lightblue' : 'lightgreen'
    }
  }

}
