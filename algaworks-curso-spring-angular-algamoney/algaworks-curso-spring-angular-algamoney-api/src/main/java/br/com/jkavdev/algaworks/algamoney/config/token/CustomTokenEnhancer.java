package br.com.jkavdev.algaworks.algamoney.config.token;

import java.util.HashMap;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import br.com.jkavdev.algaworks.algamoney.security.UsuarioSistema;

//TokenEnhancer ja nos fornece dados do token e tambem da autenticaca
// como os dados do usuario

public class CustomTokenEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

		// Retornando o user do spring envelopado pelo usuarioSistema
		UsuarioSistema usuarioSistema = (UsuarioSistema) authentication.getPrincipal();

		// Criando um map de informacoes
		// a serem adicionadasno token
		HashMap<String, Object> addInfo = new HashMap<>();
		addInfo.put("nome", usuarioSistema.getUsername());

		// Adicionandos as informacoes
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);

		return accessToken;
	}

}
