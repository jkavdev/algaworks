package br.com.jkavdev.algaworks.algamoney.model;

public enum TipoLancamento {
	
	RECEITA,
	DESPESA

}
