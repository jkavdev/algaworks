package br.com.jkavdev.algaworks.algamoney.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jkavdev.algaworks.algamoney.model.Pessoa;
import br.com.jkavdev.algaworks.algamoney.repository.pessoa.PessoaRepositoryQuery;

public interface PessoaRepository extends JpaRepository<Pessoa, Long>, PessoaRepositoryQuery {

}
