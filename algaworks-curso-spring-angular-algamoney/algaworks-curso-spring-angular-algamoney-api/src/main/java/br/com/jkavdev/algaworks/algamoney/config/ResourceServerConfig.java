package br.com.jkavdev.algaworks.algamoney.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;

//Definira o acesso aos recursos da API

@Profile("oauth-security")
@Configuration
@EnableWebSecurity
@EnableResourceServer
//Permitindo que possa configurar o acesso nos endpoints
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter{

	//provera acesso as credencias dos usuarios
	@Autowired
	private UserDetailsService userDetailsService;

	//define as configuracao para autenticacao a api
	//AuthenticationManagerBuilder eh injetado neste caso
	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		//utilizando a verificacao do usuario no banco de dados, no caso
		auth.userDetailsService(userDetailsService)
		//decodificando a senha
		.passwordEncoder(passwordEncoder());
	}
	
	//configura os acessoas aos recursos da api
	@Override
	public void configure(HttpSecurity http) throws Exception {
		//autorizando as requisicoes
		http.authorizeRequests()
				//permitindo qualquer um a acessar /categorias
				.antMatchers("/categorias").permitAll()
				//qualquer outro acesso, necessario autenticacao
				.anyRequest().authenticated()
				.and()
			//assegurando encerramento de sessao na aplicacao
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			//desabilitando recurso a pesquisar ainda
			.csrf().disable();
	}
	
	//retirando definicao de sessao
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.stateless(true);
	}
	
	//objeto responsavel por decodificar a senha
	private PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	//responsavel por prover a configuracao do acesso dos endpoints
	@Bean
	public MethodSecurityExpressionHandler createExpressionHandler() {
		return new OAuth2MethodSecurityExpressionHandler();
	}

}
