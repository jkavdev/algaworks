package br.com.jkavdev.algaworks.algamoney.security.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Util {

	// Teste da encodacao da senha

	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		System.out.println(encoder.encode("admin"));
		System.out.println(encoder.encode("maria"));
	}

}
