package br.com.jkavdev.algaworks.algamoney.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import br.com.jkavdev.algaworks.algamoney.model.Usuario;

//Temos as mesmas funcionalidades de User do spring

public class UsuarioSistema extends User {

	private static final long serialVersionUID = 1L;

	// Mas agora temos informacoes do usuario tambem
	private Usuario usuario;

	public UsuarioSistema(Usuario usuario, Collection<? extends GrantedAuthority> permissoes) {
		super(usuario.getNome(), usuario.getSenha(), permissoes);
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

}
