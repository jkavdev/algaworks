package br.com.jkavdev.algaworks.algamoney.cors;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.jkavdev.algaworks.algamoney.config.property.AlgamoneyApiProperty;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter implements Filter {

	// Injetando as propriedades da aplicacao
	@Autowired
	private AlgamoneyApiProperty algamoneyApiProperty;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		// Desenvelopando request e response para o HttpServlet
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		// Toda requisicao tera os headers de origem permitida e as credenciais
		resp.setHeader("Access-Control-Allow-Origin", algamoneyApiProperty.getOriginPermitida());
		resp.setHeader("Access-Control-Allow-Credentials", "true");

		// Se a requisicao for um teste e origem for permitida
		if ("OPTIONS".equals(req.getMethod())
				&& algamoneyApiProperty.getOriginPermitida().equals(req.getHeader("Origin"))) {
			// Adiciona headers para a operacao
			resp.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
			resp.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept");
			resp.setHeader("Access-Control-Max-Age", "3600");

			// e finalmente indica tudo ok
			resp.setStatus(HttpServletResponse.SC_OK);
		} else {
			// se nao for requisicao preflight, segue o fluxo normal
			chain.doFilter(req, resp);
		}

	}

	@Override
	public void destroy() {

	}

}
