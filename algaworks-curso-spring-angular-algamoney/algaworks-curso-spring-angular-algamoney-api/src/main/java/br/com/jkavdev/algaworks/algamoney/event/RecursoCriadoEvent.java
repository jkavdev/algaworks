package br.com.jkavdev.algaworks.algamoney.event;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationEvent;

// Teremos um evento que ao adicionar um recurso, ira configurar o header da resposta
// o spring nos fornece ApplicationEvent que configura um evento

public class RecursoCriadoEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;
	private Long codigo;
	private HttpServletResponse response;

	// Receberemos a resposta, e o codigo do recurso salvo
	public RecursoCriadoEvent(Object source, HttpServletResponse response, Long codigo) {
		super(source);
		this.response = response;
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

}
