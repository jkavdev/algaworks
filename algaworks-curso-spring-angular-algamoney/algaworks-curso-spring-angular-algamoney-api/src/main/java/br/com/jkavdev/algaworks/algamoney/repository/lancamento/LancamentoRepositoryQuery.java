package br.com.jkavdev.algaworks.algamoney.repository.lancamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.jkavdev.algaworks.algamoney.model.Lancamento;
import br.com.jkavdev.algaworks.algamoney.repository.filter.LancamentoFilter;
import br.com.jkavdev.algaworks.algamoney.repository.projection.ResumoLancamento;

public interface LancamentoRepositoryQuery {
	
	// Retornaremos agora uma pagina
	// No qual contem todas as informacoes para uma exibicao paginada
	// Como os dados a serem exibidos, quantidade de registros por pagina
	// indices das paginas navegaveis
	Page<Lancamento> filtrar(LancamentoFilter filter, Pageable pageable);
	
	// Retornaremos apenas o resumo de uma lancamento, nao todos os dados
	Page<ResumoLancamento> resumir(LancamentoFilter filter, Pageable pageable);

}
