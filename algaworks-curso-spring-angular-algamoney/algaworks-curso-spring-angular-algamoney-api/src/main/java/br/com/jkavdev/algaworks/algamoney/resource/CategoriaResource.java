package br.com.jkavdev.algaworks.algamoney.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.jkavdev.algaworks.algamoney.event.RecursoCriadoEvent;
import br.com.jkavdev.algaworks.algamoney.model.Categoria;
import br.com.jkavdev.algaworks.algamoney.repository.CategoriaRepository;

@RestController
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;

	// Mapeamento de um recurso
	// Retornara a lista de categorias como um array de objeto no json
	// e com o retorno de 200, ok, no cabecalho da requisicao
	@GetMapping
	//Realizando a autorizacao pra quem tem acesso ao recurso
	//hasAuthority('ROLE_PESQUISAR_CATEGORIA') - indica as permissoes 
	//do usuarios aceitas para acessalo
	//#oauth2.hasScope('read') objeto que oauth que le o escopo dos clientes
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public List<Categoria> listar() {
		return categoriaRepository.findAll();
	}

	// Podemos a partir de um recurso definir um cabecalho de resposta
	// Se nao houvesse dados, podemos retornar um status na falta de registros
	// Para isso o spring tem o ResponseEntity<?> no qual indica uma entidade
	// com varios metodos para a resposta
	@GetMapping("RetornoNaoEncontrado")
	public ResponseEntity<?> listarMasPondendoNaoEncontrar() {
		List<Categoria> categorias = categoriaRepository.findAll();
		// Nao havendo dados, podemos retornar um status de nao encontrado
		// Mas analisando bem, fica estranho, retornar este status, sem nehuma
		// informacao
		return !categorias.isEmpty() ? ResponseEntity.ok(categorias) : ResponseEntity.notFound().build();
	}

	@GetMapping("RetornoSemConteudo")
	public ResponseEntity<?> listarMasPodendoNaoTerConteudo() {
		List<Categoria> categorias = categoriaRepository.findAll();
		// podemos ainda retornar o status requisicao ocorreu com exito
		// mas nao houve retorno de dados
		// por isso a analise sempre para qual status retornar
		return !categorias.isEmpty() ? ResponseEntity.ok(categorias) : ResponseEntity.noContent().build();
	}

	// Podemos retorar o status tambem com @ResponseStatus
	// Quando criamos algum recurso, por padrao rest forneceremos dados de acesso
	// para isso podemos usar o HttpServletResponse response para indicalo
	// Informando ao spring para realizar a validacao do objeto com @Valid
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_CATEGORIA') and #oauth2.hasScope('write')")
	public ResponseEntity<Categoria> criar(@Valid @RequestBody Categoria categoria, HttpServletResponse response) {
		Categoria categoriaSalva = categoriaRepository.save(categoria);

		// pegando o contexto atual: categorias
		// adicionando: {codigo} e seu valor
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
//				.buildAndExpand(categoriaSalva.getCodigo()).toUri();
		// adicionando a uri criada, a resposta da requisicao
//		response.setHeader("Location", uri.toASCIIString());

		// Podemos tambem retornar o recurso que foi criado, indicando o acesso a ele e
		// o proprio recurso
//		return ResponseEntity.created(uri).body(categoriaSalva);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, categoriaSalva.getCodigo()));

		return ResponseEntity.status(HttpStatus.CREATED).body(categoriaSalva);
	}

	// Como teremos que acessar o recurso criado, criamos a funcionalidade de acesso
	// a um recurso
	// recebendo o codigo por requisicao
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public Categoria buscarPeloCodigo(@PathVariable Long codigo) {
		return categoriaRepository.findOne(codigo);
	}

}
