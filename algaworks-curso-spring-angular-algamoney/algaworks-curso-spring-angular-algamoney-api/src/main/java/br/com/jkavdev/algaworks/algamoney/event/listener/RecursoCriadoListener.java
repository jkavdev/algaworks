package br.com.jkavdev.algaworks.algamoney.event.listener;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.jkavdev.algaworks.algamoney.event.RecursoCriadoEvent;

// Configurando um listener, para a adicao do header nas resposta das requisicoes

@Component
public class RecursoCriadoListener implements ApplicationListener<RecursoCriadoEvent> {

	// onApplicationEvent, recebe o evento
	// e apartir do evento configuramos o header
	@Override
	public void onApplicationEvent(RecursoCriadoEvent event) {
		HttpServletResponse response = event.getResponse();
		Long codigo = event.getCodigo();

		adicionarHeaderLocation(response, codigo);
	}

	// Realiza a adicao do header location
	private void adicionarHeaderLocation(HttpServletResponse response, Long codigo) {
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
				.path("/{codigo}")
				.buildAndExpand(codigo)
				.toUri();
		response.setHeader("Location", uri.toASCIIString());
	}

}
