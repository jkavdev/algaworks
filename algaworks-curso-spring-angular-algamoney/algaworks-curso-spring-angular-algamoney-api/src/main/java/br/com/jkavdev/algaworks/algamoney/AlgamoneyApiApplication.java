package br.com.jkavdev.algaworks.algamoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import br.com.jkavdev.algaworks.algamoney.config.property.AlgamoneyApiProperty;

//classe de inicial de configuracao do spring boot

@SpringBootApplication
// Habilitando a definicao de propriedades da aplicacao
@EnableConfigurationProperties(AlgamoneyApiProperty.class)
public class AlgamoneyApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlgamoneyApiApplication.class, args);
	}
}
