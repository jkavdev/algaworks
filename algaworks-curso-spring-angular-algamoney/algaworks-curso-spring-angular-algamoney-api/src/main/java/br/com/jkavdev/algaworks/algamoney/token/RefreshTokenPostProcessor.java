package br.com.jkavdev.algaworks.algamoney.token;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import br.com.jkavdev.algaworks.algamoney.config.property.AlgamoneyApiProperty;

//Controlador que ira adicionar o cookie com o refreshToken
//ResponseBodyAdvice - interface que interceptara os recursos com OAuth2AccessToken na resposta
//No caso sao nosso geradores de tokens
@ControllerAdvice
public class RefreshTokenPostProcessor implements ResponseBodyAdvice<OAuth2AccessToken> {
	
	// Injetando as propriedades da aplicacao
	@Autowired
	private AlgamoneyApiProperty algamoneyApiProperty;

	// Indicamos o que interceptar
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> arg1) {
		return returnType.getMethod().getName().equals("postAccessToken");
	}

	// indicamos como o refreshToken sera enviado, no caso criaremos o cookie
	@Override
	public OAuth2AccessToken beforeBodyWrite(OAuth2AccessToken body, MethodParameter returnType,
			MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConvertedType,
			ServerHttpRequest request, ServerHttpResponse response) {

		// precisamos da requisicao e resposta para adicionar o cookie
		HttpServletRequest req = ((ServletServerHttpRequest) request).getServletRequest();
		HttpServletResponse resp = ((ServletServerHttpResponse) response).getServletResponse();

		// convertendo o body, conteudo gerado da resposta
		DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) body;

		// o refreshToken da requisicao
		String refreshToken = body.getRefreshToken().getValue();

		adicionarRefreshTokenNoCookie(refreshToken, req, resp);
		removerRefreshTokenDoBody(token);

		return body;
	}

	private void removerRefreshTokenDoBody(DefaultOAuth2AccessToken token) {
		// com isso retirara o refreshToken do objeto retornado
		// com o accessToken e as outras informacoes
		// providas pelo OAuth 2
		token.setRefreshToken(null);
	}

	private void adicionarRefreshTokenNoCookie(String refreshToken, HttpServletRequest req, HttpServletResponse resp) {
		// cria um cookie normal, servlet
		Cookie refreshTokenCookie = new Cookie("refreshToken", refreshToken);
		// acesso apenas em http
		refreshTokenCookie.setHttpOnly(true);
		refreshTokenCookie.setSecure(algamoneyApiProperty.getSeguranca().isEnableHttps());
		// qual o caminho do cookie
		refreshTokenCookie.setPath(req.getContextPath() + "/oauth/token");
		// tempo de vida 1 dia
		refreshTokenCookie.setMaxAge(2592000);
		// adicionando o cookie na resposta
		resp.addCookie(refreshTokenCookie);
	}

}
