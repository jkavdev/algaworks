package br.com.jkavdev.algaworks.algamoney.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import br.com.jkavdev.algaworks.algamoney.config.token.CustomTokenEnhancer;

//Configurara os clientes e seus niveis de acesso, junto com as definicoes dos tokens

//Indicando profile
//No caso sera quando o profile de oauth tiver ativo
@Profile("oauth-security")
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	// gerenciador das autorizacoes
	// ele tem acesso as informacoes do usuario, owner
	@Autowired
	private AuthenticationManager authenticationManager;

	// Define os clientes com acesso aos recursos
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
				//credenciais do cliente
				.withClient("angular")
				.secret("angular")
				//escopo, quais tipos de acoes estara disponivel para o cliente 
				.scopes("read", "write")
				//fluxo, onde o cliente recebe as credenciais do usaurio - owner
				//apenas quando o cliente eh de inteira confianca, pois
				//estamos dando os dados do usuario
				//refresh_token - disponibilizara a possibilidade de renovar o token
				.authorizedGrantTypes("password", "refresh_token")
				//tempo de validade do token, 30 min
				.accessTokenValiditySeconds(1800)
				//tempo de validade do refresh_token, 1 dia
				.refreshTokenValiditySeconds(3600 * 24)
			.and()
				.withClient("mobile")
				.secret("mobile")
				.scopes("read")
				.authorizedGrantTypes("password", "refresh_token")
				.accessTokenValiditySeconds(1800)
				.refreshTokenValiditySeconds(3600 * 24);
	}

	// Definira onde e como sao criado os tokens de validacao
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		//Sera adicionado ao dados do token o nome do usuario
		//Para tal, TokenEnhancerChain recebe um customizador de tokens e um converter
		//Ele gerencia a customizacao do token
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));
		endpoints
			//definicao de onde estao os tokens
			.tokenStore(tokenStore())
			//configurando o converter do token
//			.accessTokenConverter(accessTokenConverter())
			//Definindo o customizador de token
			.tokenEnhancer(tokenEnhancerChain)
			//nao reutilizara o refresh_token, quando expirado
			//	ou buscando um access token, outro refresh_token sera gerado
			.reuseRefreshTokens(false)
			//validador
			.authenticationManager(authenticationManager);
	}


	//sera um bean, podendo ser acessado a qualquer
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		//o spring security ja nos fornece uma classe que realiza a conversao do token
		JwtAccessTokenConverter tokenConverter = new JwtAccessTokenConverter();
		//definicao da chave de verificacao do token
		tokenConverter.setSigningKey("jkavdev");
		
		return tokenConverter;
	}

	//com o jwt, nao eh necessario armazenar o token, pois
	//o proprio token ja possui toda a informacao necessaria
	@Bean
	public TokenStore tokenStore() {
		//classe do spring security que prove esta funcionalidade, 
		//passando o converter do token
		return new JwtTokenStore(accessTokenConverter());
	}

	//Para customizar o token, criamos uma classe para a alteracao
	@Bean
	public CustomTokenEnhancer tokenEnhancer() {
		return new CustomTokenEnhancer();
	}
	
}
