package br.com.jkavdev.algaworks.algamoney.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jkavdev.algaworks.algamoney.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	//Pode ou nao haver o usuario
	Optional<Usuario> findByEmail(String email);

}
