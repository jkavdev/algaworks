package br.com.jkavdev.algaworks.algamoney.token;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.catalina.util.ParameterMap;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

// Filtro que ficara por pegar a requisicao do novo token, 
// e adicionar o refreshToken do cookie na requisicao 

// @Order(Ordered.HIGHEST_PRECEDENCE) - indicamos que este componente tem 
// uma precendencia muito alto, executa-lo antes de qualquer outra acao

// Filter do servlet, normal 

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RefreshTokenCookiePreProcessor implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		// obtemos a requisicao 
		HttpServletRequest req = (HttpServletRequest) request;

		//assegurando que eh a requisicao de novo token
		//caminho da requisicao /oauth/token
		//se o grant_type eh refresh_token
		//e se ha cookies na requisicao
		if ("/oauth/token".equalsIgnoreCase(req.getRequestURI())
				&& "refresh_token".equals(req.getParameter("grant_type")) 
				&& req.getCookies() != null) {

			//pego os cookies
			for (Cookie cookie : req.getCookies()) {
				// cookie com refresh_token
				if (cookie.getName().equals("refreshToken")) {
					//retorna o valor do cookie, valor refresh_token 
					String refreshToken = cookie.getValue();
					//como a requisicao eh um objeto que nao pode alterar
					//seu dados criaremos um objeto que obtera os dados da requisicao
					//adicionando o valor do cookie a requisicao
					req = new MyServletRequestWrapper(req, refreshToken);
				}
			}
		}
		
		//como eh um filter, eh importante
		//da prosseguimento a outras atividades
		chain.doFilter(req, response);

	}

	//classe que obtera a nova requisicao com o refresh_token
	static class MyServletRequestWrapper extends HttpServletRequestWrapper {

		private String refreshToken;

		public MyServletRequestWrapper(HttpServletRequest req, String refreshToken) {
			super(req);
			this.refreshToken = refreshToken;
		}

		@Override
		public Map<String, String[]> getParameterMap() {
			//retorna todos os parametros da requisicao atual
			ParameterMap<String, String[]> map = new ParameterMap<>(getRequest().getParameterMap());
			//adicionamos o valor do refresh_token aos parametros
			map.put("refresh_token", new String[] { refreshToken });
			map.setLocked(true);

			return map;
		}

	}

	@Override
	public void destroy() {

	}

}
