# Realizando requisição de token #

* Definimos Basic Auth, credenciais do usuário
	
	username=admin
	password=admin

* Adicionamos no corpo da requisição os dados para acesso ao token

	[
		{"key":"client","value":"angular","description":""},
		{"key":"username","value":"admin","description":""},
		{"key":"password","value":"admin","description":""},
		{"key":"grant_type","value":"password","description":""}
	]	

* Requisição montada, depois da definição dos parâmetros de acesso ao token 

	POST /oauth/token HTTP/1.1
	Host: localhost:8080
	Content-Type: application/x-www-form-urlencoded
	Authorization: Basic YW5ndWxhcjphbmd1bGFy
	Cache-Control: no-cache
	Postman-Token: 069fa65e-defa-adc2-d50e-607e6d67e5ab
	client=angular&username=admin&password=admin&grant_type=password
	
* Será retornado no corpo da requisição os dados do acesso, token, tempo de vida e mais...

	{
	    "access_token": "f2fa16ee-b23a-459d-8a70-9ab40ad4ee0b",
	    "token_type": "bearer",
	    "expires_in": 1656,
	    "scope": "read write"
	}	
	
# Realizando acesso a recursos com o TOKEN #
* Definir o token gerado no header da requisição

	GET /lancamentos HTTP/1.1
	Host: localhost:8080
	Authorization: bearer f2fa16ee-b23a-459d-8a70-9ab40ad4ee0b
	Cache-Control: no-cache
	Postman-Token: 43ba36e9-657a-c0a4-e8ee-4246e3b58144
	
# Requerindo novo TOKEN com o REFRESH_TOKEN#
* Realizar os mesmos passos de `Realizando requisição de token`
	
* Mas agora será retornado junto com os dados anteriormente descritos o `refresh_token`

	{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MTU1OTM4MjEsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9ST0xFIl0sImp0aSI6IjRlOWY1MDZiLTQ2NDAtNDQ5OS1hOTZkLTlkNzA5ZDI5ZTA4OSIsImNsaWVudF9pZCI6ImFuZ3VsYXIiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXX0.8pwGO5EN5sgMvvnbv_Hg9zQEK92B0hlEJcYUsEdOQRY",
    "token_type": "bearer",
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI0ZTlmNTA2Yi00NjQwLTQ0OTktYTk2ZC05ZDcwOWQyOWUwODkiLCJleHAiOjE1MTU2ODAyMDEsImF1dGhvcml0aWVzIjpbIlJPTEVfUk9MRSJdLCJqdGkiOiIzMGExMDI4Yi05OWJiLTRlZGYtOTI0OS04YzFkZDJmNjRhNGUiLCJjbGllbnRfaWQiOiJhbmd1bGFyIn0.W8cb7E5honLGJnEVN9BA1O68iEjsLE72qqLO4OPm_YM",
    "expires_in": 19,
    "scope": "read write",
    "jti": "4e9f506b-4640-4499-a96d-9d709d29e089"
}

* Em posse do `refresh_token` podemos realizar outra requisição para obter um novo `access token`
* Não precisar informar os dados do usuários, mas temos que definir as credenciais do cliente
	
	username=angular
	password=angular

* Adicionamos no corpo da requisição os dados para acesso ao novo `token`
* Definimos o `grant_type` que é `refresh_token` e o valor `refresh_token`

	[
		{
			"key":"grant_type",
			"value":"refresh_token",
			"description":""
		},
		{
			"key":"refresh_token",
			"value":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIyNmEwNzQyNi1hMWExLTRjYzMtYjI0MS0zNGZhMjA5OWFkM2YiLCJleHAiOjE1MTU2NzgzODcsImF1dGhvcml0aWVzIjpbIlJPTEVfUk9MRSJdLCJqdGkiOiIzZTIzMjVlNC00ZjYyLTQ0Y2EtOTYwYi0zNTE3YzVlMjM4ZDEiLCJjbGllbnRfaWQiOiJhbmd1bGFyIn0.4SFvysmDObrA-84zIIXT4MJK1oOtZpY_xMTEhdRm0XY",
			"description":""
		}
	]

* Requisição montada, depois da definição dos parâmetros 

	POST /oauth/token HTTP/1.1
	Host: localhost:8080
	Content-Type: application/x-www-form-urlencoded
	Authorization: Basic YW5ndWxhcjphbmd1bGFy
	Cache-Control: no-cache
	Postman-Token: 43fbd547-587b-cc88-a8a7-a1f52b9fd274
	grant_type=refresh_token&refresh_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIyNmEwNzQyNi1hMWExLTRjYzMtYjI0MS0zNGZhMjA5OWFkM2YiLCJleHAiOjE1MTU2NzgzODcsImF1dGhvcml0aWVzIjpbIlJPTEVfUk9MRSJdLCJqdGkiOiIzZTIzMjVlNC00ZjYyLTQ0Y2EtOTYwYi0zNTE3YzVlMjM4ZDEiLCJjbGllbnRfaWQiOiJhbmd1bGFyIn0.4SFvysmDObrA-84zIIXT4MJK1oOtZpY_xMTEhdRm0XY
	
* Será retornado no corpo da requisição os dados do novo `access token`

	{
	    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MTU1OTQ1NjAsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9ST0xFIl0sImp0aSI6IjIzYTEzMzJlLThmODItNDQzZS1hZTZmLWE3ZTA1MWM1NGEwNSIsImNsaWVudF9pZCI6ImFuZ3VsYXIiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXX0.69GjQmZsgU8U0Cc6bmqQkqHZoriseLCwiiUr5DjDoDI",
	    "token_type": "bearer",
	    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIyM2ExMzMyZS04ZjgyLTQ0M2UtYWU2Zi1hN2UwNTFjNTRhMDUiLCJleHAiOjE1MTU2ODA5NDAsImF1dGhvcml0aWVzIjpbIlJPTEVfUk9MRSJdLCJqdGkiOiIzMjg0YWEwNC00MDZmLTQyMDktYTA5MS00YTY2OWUxYzQ2MDMiLCJjbGllbnRfaWQiOiJhbmd1bGFyIn0.5VhwoMnE2kJa3Am8B26ep-ePIHScm1QZLkAjRrEoCtg",
	    "expires_in": 19,
	    "scope": "read write",
	    "jti": "23a1332e-8f82-443e-ae6f-a7e051c54a05"
	}
	
# Adicionando REFRESH_TOKEN no COOKIE da requisição#
* Ao fazer os mesmos passos de `Realizando requisição de token`
* Será apenas retornado os dados do `access token`

	{
	    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MTU2MDQ0ODIsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9ST0xFIl0sImp0aSI6IjNhOWVmNGVjLTBlYWQtNDZhNy1hOThiLTU3MDZlNTY1YzhkMSIsImNsaWVudF9pZCI6ImFuZ3VsYXIiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXX0.u_92Y--6RtIjh4Cyuqgxdn8Xk967IAAHMRQ5qQv7YP0",
	    "token_type": "bearer",
	    "expires_in": 19,
	    "scope": "read write",
	    "jti": "3a9ef4ec-0ead-46a7-a98b-5706e565c8d1"
	}	

* E o `Cookie` será adicionado aos `Cookies` da requisição

	Set-Cookie →refreshToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIzYTllZjRlYy0wZWFkLTQ2YTctYTk4Yi01NzA2ZTU2NWM4ZDEiLCJleHAiOjE1MTU2OTA4NjIsImF1dGhvcml0aWVzIjpbIlJPTEVfUk9MRSJdLCJqdGkiOiJjNzRmMGFlOS00MzcxLTQxOTItYTA5NS01ZTQ3NmQ2Y2FmMGUiLCJjbGllbnRfaWQiOiJhbmd1bGFyIn0.jlDAyardqShg8389YmJtLIr5qreMcZV0w2E2LbTXMvI; Max-Age=2592000; Expires=Fri, 09-Feb-2018 17:14:22 GMT; Path=/oauth/token; HttpOnly
	
* Requisição montada

	POST /oauth/token HTTP/1.1
	Host: localhost:8080
	Content-Type: application/x-www-form-urlencoded
	Authorization: Basic YW5ndWxhcjphbmd1bGFy
	Cache-Control: no-cache
	Postman-Token: 9955b8e2-22c1-61e6-80f1-083702770ac0
	client=angular&username=admin&password=admin&grant_type=password		
	
# Instalando o HEROKU localmente#
* Instalar o Heroku
* * Criar conta no Heroku.
* * Ter o Java 8 instalando localmente.
* * Ter o Maven 3 instalando localmente.
* * Baixar e instalar o HerokuCLI.
* * Depois de instalar o HerokuCLI
* * * Executar o seguinte comando passando seu usuário e senha do Heroku: `heroku login`
	
# Realizando o deploy da aplicação no HEROKU
* Criando a aplicação no Heroku: `heroku create spring-angular-algamoney-api`	
* Adicionando fornecedor de armazenamento: `heroku addons:create jawsdb -a spring-angular-algamoney-api`	
* Com isso será disponibilizado o banco e url para acesso: `heroku config:get JAWSDB_URL -a spring-angular-algamoney-api`
* Criando as váriaveis no Heroku de acesso ao banco de dados `heroku config:set JDBC_DATABASE_URL=o3iyl77734b9n3tg.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306/z2znijc24mqtizcn JDBC_DATABASE_USERNAME=hhhmb0ja2r9g99d3 JDBC_DATABASE_PASSWORD=:nml9wy0utr93e782 -a spring-angular-algamoney-api`	
* Exibir as definições da aplicação: `heroku config -a spring-angular-algamoney-api`
* O Heroku obriga ter um arquivo na raiz do projeto com as seguintes definições:

* O Heroku solicita
	
	web: java -Dserver.port=$PORT 
	$JAVA_OPTS 
	-jar target/algamoney*.jar
	
* Estamos informando qual application.properties sera configurado, no caso sera -prod 
	
	-Dspring.profiles.active=prod 
	
* Arquivo final

	web: java -Dserver.port=$PORT -Dspring.profiles.active=prod $JAVA_OPTS -jar target/algamoney*.jar
	
* Enviando alterações realizadas no projeto para o Heroku: `git push heroku master`

* Acessando logs da aplicação no Heroku: `heroku logs --tail`	
	
	
	
	
	
	
		