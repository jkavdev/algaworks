package br.com.jkavdev.algaworks.patrimonio.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.jkavdev.algaworks.patrimonio.model.Item;
import br.com.jkavdev.algaworks.patrimonio.repository.ItemRepository;

@RestController
@CrossOrigin("${origem-permitida}")
public class IitemResource {

	@Autowired
	private ItemRepository itemRepository;

	@GetMapping("/itens")
	public List<Item> listar() {
		List<Item> itens = itemRepository.findAll();
		itens.forEach(System.out::println);
		return itens;
	}

	@PostMapping("/itens")
	public Item adicionar(@RequestBody @Valid Item item) {
		System.out.println("Salvando " + item);
		return itemRepository.save(item);
	}

}
