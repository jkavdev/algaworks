* Testes realizados no Postman

* Para testar recurso de lista itens
 
	GET /itens HTTP/1.1
	Host: localhost:8080
	Cache-Control: no-cache
	Postman-Token: 381f0339-4aa2-726e-2d67-138bb60341e8
	 
* Para testar recurso de adicionar um item

	POST /itens HTTP/1.1
	Host: localhost:8080
	Content-Type: application/json
	Cache-Control: no-cache
	Postman-Token: 5426496c-5a2c-58a2-75d5-491fe1949993
	{
	    "etiqueta": "AD3415",
	    "descricao": "Monitor 25pol",
	    "dataAquisicao": "2018-10-28"
	}
	 
	 
* Testando cross-origin

* Para realizar o teste, vá até /docs

* Instalando o express

	npm install express
	
* Rodando o serviço

	node server.js	 
	
* Executando teste de cross-origin, acesse

	http://localhost:4200	
	
* Iniciando o projeto WEB

* Instalar o node
* Instalar o Angular CLI
	
	npm install -g @angular/cli
	ng -v

* Se a versão local for menor que a versão local do angular/cli

	npm install --save-dev @angular/cli@latest

* Criando o projeto

	ng new algaworks-oficina-angular-rest-spring-boot-patrimonio-ui

* Rodando o projeto

	ng serve	

* Criando um componente com angular/cli

	ng g c navbar --spec=false	

* Instalando o PrimeNG, localmente

	npm install primeng --save	