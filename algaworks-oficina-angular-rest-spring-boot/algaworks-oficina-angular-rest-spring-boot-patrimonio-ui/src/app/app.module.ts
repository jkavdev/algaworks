import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Definindo DataTableModule como dependecia para o projeto, trara a conjunto de tags
//de tabelas
import { DataTableModule } from 'primeng/primeng'

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ItemCadastroComponent } from './item-cadastro/item-cadastro.component';
//Decorator com informacoes sobre o modulo
//Definicao de todas as informacoes da aplicacao
//declarations - Define os componentes da aplicacao
//imports - define as dependencias do AppModule
//bootstrap - configura o componente principal da aplicacao
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ItemCadastroComponent
  ],
  imports: [
    BrowserModule,

    DataTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
