import { Component } from '@angular/core';

// Responsavel pela view

// Decorator com informacoes do componente
// templateUrl - pagina do componente
// styleUrls - arquivos de estilo da pagina
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
